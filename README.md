# CS346-project-fall-2023

## Wiki page
https://git.uwaterloo.ca/k4tieu/cs346-project-fall-2023/-/wikis

# Diagram
https://git.uwaterloo.ca/k4tieu/cs346-project-fall-2023/-/wikis/Design

## To download the application:
Link to installer: https://git.uwaterloo.ca/k4tieu/cs346-project-fall-2023/-/tree/main/application_installer?ref_type=heads

##  To run the server:
Run this command to pull the docker image for the server: docker pull tk2k2/cs346-server:latest

Run this command to run the docker image for the server: docker run -d -p 8080:8080 tk2k2/cs346-server

##  To generate JAR file for the server:
https://www.jetbrains.com/help/idea/compiling-applications.html#package_into_jar


## To run the app:
After running the server from the Docker image, just run the application created from the installer.

## Meeting Minutes
https://git.uwaterloo.ca/k4tieu/cs346-project-fall-2023/-/wikis/Meeting%20Minutes
